#!/bin/sh

#Prerequisitos:
#1) Instalar node
#2) Instalar npm
#3) Instalar git

#1. Instalación de Angular y Schematics de forma global.

yes N | npm install -g @angular/cli@v11-lts @angular-devkit/schematics-cli@v11-lts

#2. Descarga y preparación del repositorio "repositorioeshop-actualizado".
echo "paso 2"
wget https://gitlab.com/1jorge.echeverria/repositorioeshop-actualizado/-/archive/main/repositorioeshop-actualizado-main.tar.gz
tar -xzvf repositorioeshop-actualizado-main.tar.gz
#git clone https://gitlab.com/1jorge.echeverria/repositorioeshop-actualizado
cd repositorioeshop-actualizado-main
cd repositorio
npm install

#3. Compilación de repositorioeshop.
echo "paso 3"
npm run build

#4. Creación y preparación de ProyectoNuevo.
echo "paso 4"
cd ../..
yes | ng new ProyectoNuevo --routing --style=scss
cd ProyectoNuevo
yes | npm install @angular/cdk@v11-lts @angular/material@v11-lts @angular/flex-layout@9.0.0-beta.31

#5. Ejecución de Schematics en ProyectoNuevo.
echo "paso 5"
schematics ../repositorioeshop-actualizado-main/repositorio/src/collection.json:ng-add --proyecto=ProyectoNuevo --catalogo --general --categorias --e_shop  --dry-run=false

#6. Corrección de tsconfig en ProyectoNuevo para evitar errores sintacticos que imposibilitan el despliegue.
echo "paso 6"
rm -Rf tsconfig.json
ls
#git clone https://gitlab.com/1jorge.echeverria/repositorioeshop-actualizado/snippets/2367104.git
wget https://gitlab.com/1jorge.echeverria/repositorioeshop-actualizado/-/snippets/2367104/raw/main/tsconfig.json
#?inline=false
#mv 2367104/tsconfig.json .
#rm -Rf 2367104/