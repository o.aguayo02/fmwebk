<h1 align="center">
  FMweb-K
</h1>

## Construcción del proyecto
```
docker build --no-cache --progress plain -t fmwebk .\ 
```

### Ejecución del proyecto
```
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock --name gestorvariabilidad -t fmwebk
```



### Nota
```
Se modificaron las credenciales de acceso al sensor de ultrasonido por seguridad
```

