FROM openjdk:17-jdk-alpine
#EXPOSE 81
WORKDIR /fmwebk
COPY . .
RUN ls
RUN apk update && \
    apk add docker
RUN ["./mvnw" ,"package"]
RUN ["chmod", "+x", "init.sh"]
ENTRYPOINT ["./init.sh"]