package com.fmwebk.controloop;

import com.fmwebk.model.PuntoVariacion;
import com.fmwebk.model.Sensor;
import com.fmwebk.model.TipoSensor;
import com.fmwebk.utils.ConectorIoT;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GestorVariabilidad {
    private Sensor sensor;

    public GestorVariabilidad() {
        init();
    }

    public void init(){
        this.sensor = new Sensor(TipoSensor.ULTRASONIDO,"url_telemetria",
                "url_autenticacion","token","token_refresco");
        PuntoVariacion casoBase= new PuntoVariacion("./puntos-variacion/caso-base/","casobase","ContBase","8080","8080",30);
        PuntoVariacion casoBuscadorBasico = new PuntoVariacion("./puntos-variacion/caso-buscador-basico/","casobuscadorbasico","ContBuscadorBasico","8081","8081",90);
        this.sensor.agregarPuntoVariacion(casoBase);
        this.sensor.agregarPuntoVariacion(casoBuscadorBasico);
    }

    //método que estará escuchando constantemente a dispositivo IoT
    public void monitorear() throws IOException, InterruptedException {
        if(!ConectorIoT.getStatusSensor(this.sensor)) {
            analizar(ConectorIoT.getDistance(this.sensor));
        }
    }
    //metodo que buscara que reconfiguración se adecúa a las necesidades
    public void analizar(int condicion) throws IOException, InterruptedException {
        PuntoVariacion puntoVariacion = this.buscarPuntoVariacion(condicion);
        if(puntoVariacion!=null) {
            if (!existePunteVariacion(puntoVariacion)) {
                planificar(puntoVariacion);
            }
        }
    }

    // método que planifica como se ejecutará la reconfiguración
    //busca si hay otro punto de variación y lo retorna
    public void planificar(PuntoVariacion puntoVariacion) throws IOException, InterruptedException {
     PuntoVariacion puntoVariacionActivo = this.buscarContenedorEjecucion();
     ejecutar(puntoVariacionActivo,puntoVariacion);
    }

    //método que ejecuta el cambio
    public void ejecutar(PuntoVariacion puntoVariacionActivo, PuntoVariacion puntoVariacionNuevo) throws IOException, InterruptedException {
        Process activarPuntoVariacion = Runtime.getRuntime().exec(puntoVariacionNuevo.getComandoDockerRun());
         if(puntoVariacionActivo != null) {
            Thread.sleep(30 * 1000);
            Process desactivarPuntoVariacionAntiguo = Runtime.getRuntime().exec(puntoVariacionActivo.getComandoDockerStop());
            }

    }
    //nodo conocimiento compartido
    public void almacenarRegistro(){

    }
    private PuntoVariacion buscarPuntoVariacion(int condicion){
        PuntoVariacion puntoVariacionCondicion= null;
        for(PuntoVariacion puntoVariacion : this.sensor.getPuntosVariacion()){
            if(puntoVariacion.cumpleReglaAdaptacion(condicion)){
                puntoVariacionCondicion = puntoVariacion;
                break;
            }
        }
        return puntoVariacionCondicion;
    }

    private PuntoVariacion buscarContenedorEjecucion() throws IOException {
        InputStream inputstream = this.obtenerContenedoresEjecucion();
        PuntoVariacion puntoVariacionEjecucion = null;
        byte[] contents = new byte[1024];
        int bytesRead = 0;
        String data="";
        while((bytesRead = inputstream.read(contents)) != -1) {
            data = new String(contents, 0, bytesRead).replace("/", "").replace(".", "").replace("\"", "").replace("->", "").replace(":", "");
        }
        for(PuntoVariacion puntoVariacion : this.sensor.getPuntosVariacion()) {
            String regex = ".*" + puntoVariacion.getImagenDocker() + ".*";
            if(validarRegex(false, data, regex)){
                puntoVariacionEjecucion = puntoVariacion;
                break;
            }
        }

        return puntoVariacionEjecucion;
    }

    private InputStream obtenerContenedoresEjecucion() throws IOException {
        Process process = Runtime.getRuntime().exec("docker ps");
        InputStream inputstream = process.getInputStream();
        return inputstream;
    }
    private boolean existePunteVariacion(PuntoVariacion puntoVariacion) throws IOException {
        InputStream inputstream = this.obtenerContenedoresEjecucion();
        byte[] contents = new byte[1024];
        int bytesRead = 0;
        String data = "";
        while((bytesRead = inputstream.read(contents)) != -1) {
            data= new String(contents, 0, bytesRead).replace("/","").replace(".","").replace("\"","").replace("->","").replace(":","");
        }
        String regex = ".*"+puntoVariacion.getImagenDocker()+".*";
        return this.validarRegex(false,data,regex);
    }

    private boolean validarRegex(boolean condicion, String data, String regex){
        System.out.println(data);
        Pattern pattern = Pattern.compile(regex);
        Matcher mat = pattern.matcher(data);
        if(mat.find()){
            condicion = true;
        }
        return condicion;
    }

}
