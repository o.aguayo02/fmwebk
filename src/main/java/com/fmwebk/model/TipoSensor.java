package com.fmwebk.model;

public enum TipoSensor {

    ACELEROMETRO("Acelerómetro"),
    HUMEDAD("Humedad"),
    INFRARROJO("Infrarrojo"),
    LUZ("Luz"),
    MOVIMIENTO("Movimiento"),
    ULTRASONIDO("Ultrasonido");

    private String nombreSensor;

    TipoSensor(String nombreSensor){
        this.nombreSensor = nombreSensor;
    }

    public String getNombreSensor() {
        return nombreSensor;
    }
}
