package com.fmwebk.model;

import java.util.ArrayList;
import java.util.List;

public class Sensor {
private TipoSensor tipoSensor;
private List<PuntoVariacion> puntosVariacion;
private String url_telemetria;
private String url_autenticacion;
private String token;
private String refresh_token;

    public Sensor(TipoSensor tipoSensor, String url_telemetria, String url_autenticacion, String token, String refresh_token) {
        this.tipoSensor = tipoSensor;
        this.url_telemetria = url_telemetria;
        this.url_autenticacion = url_autenticacion;
        this.token = token;
        this.refresh_token = refresh_token;
        this.puntosVariacion= new ArrayList<>();
    }

    public Sensor(TipoSensor tipoSensor) {
        this.tipoSensor = tipoSensor;
    }

    public TipoSensor getTipoSensor() {
        return tipoSensor;
    }

    public void setTipoSensor(TipoSensor tipoSensor) {
        this.tipoSensor = tipoSensor;
    }

    public List<PuntoVariacion> getPuntosVariacion() {
        return puntosVariacion;
    }

    public void setPuntosVariacion(List<PuntoVariacion> puntosVariacion) {
        this.puntosVariacion = puntosVariacion;
    }

    public String getUrl_telemetria() {
        return url_telemetria;
    }

    public void setUrl_telemetria(String url_telemetria) {
        this.url_telemetria = url_telemetria;
    }

    public String getUrl_autenticacion() {
        return url_autenticacion;
    }

    public void setUrl_autenticacion(String url_autenticacion) {
        this.url_autenticacion = url_autenticacion;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public void agregarPuntoVariacion(PuntoVariacion puntoVariacion){
        this.puntosVariacion.add(puntoVariacion);
    }
}
