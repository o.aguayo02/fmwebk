package com.fmwebk.model;

public class PuntoVariacion {
    private String direccionImagen;
    private String imagenDocker;
    private String nombreContenedor;
    private String puertoContenedor;
    private String puertoHost;
    private int reglaAdaptacionSensor;

    public PuntoVariacion(String direccionImagen, String imagenDocker, String nombreContenedor, String puertoContenedor, String puertoHost, int reglaAdaptacionSensor) {
        this.direccionImagen = direccionImagen;
        this.imagenDocker = imagenDocker;
        this.nombreContenedor = nombreContenedor;
        this.puertoContenedor = puertoContenedor;
        this.puertoHost = puertoHost;
        this.reglaAdaptacionSensor = reglaAdaptacionSensor;
    }

    public String getImagenDocker() {
        return imagenDocker;
    }

    public void setImagenDocker(String imagenDocker) {
        this.imagenDocker = imagenDocker;
    }

    public String getNombreContenedor() {
        return nombreContenedor;
    }

    public void setNombreContenedor(String nombreContenedor) {
        this.nombreContenedor = nombreContenedor;
    }

    public String getComandoDockerRun() {
        String comando= "docker run --rm -p "+ puertoHost+":"+puertoContenedor+" --name "+nombreContenedor+" -t "+imagenDocker;
        return comando;
    }
    public String getComandoDockerStop(){
        String comando = "docker stop "+nombreContenedor;
        return comando;
    }
    public String getComandoDockerBuild(){
        String comando ="docker build -t "+imagenDocker+" "+direccionImagen;
        return comando;
    }

    public boolean cumpleReglaAdaptacion(int valorSensor){
        int diferenciaValoresSensor= Math.abs(reglaAdaptacionSensor-valorSensor);
        if(diferenciaValoresSensor < 20){
            return true;
        }
        else{
            return false;
        }
    }
    public int getReglaAdaptacionSensor() {
        return reglaAdaptacionSensor;
    }

    public void setReglaAdaptacionSensor(int reglaAdaptacionSensor) {
        this.reglaAdaptacionSensor = reglaAdaptacionSensor;
    }

}
