package com.fmwebk.motor;

import com.fmwebk.controloop.GestorVariabilidad;
import jdk.swing.interop.SwingInterOpUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;

@SpringBootApplication
@EnableScheduling
public class FmwebkApplication {

	public static void main(String[] args) throws IOException, InterruptedException {
		SpringApplication.run(FmwebkApplication.class, args);
		Process activarNginx = Runtime.getRuntime().exec("docker run --rm -p 80:80 --name nginx -t nginx");
	}

	@Scheduled(fixedRate = 100000)
	public void tarea() throws IOException, InterruptedException {
		GestorVariabilidad gestorVariabilidad = new GestorVariabilidad();
		gestorVariabilidad.monitorear();

	}
}
