package com.fmwebk.utils;
import com.fmwebk.model.Sensor;
import org.json.JSONObject;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import org.json.JSONArray;


public class ConectorIoT {


    public static boolean getStatusSensor(Sensor sensor) throws IOException, InterruptedException {
        String response_t1 = getTimeLastMedition(getResponse(sensor));
        Thread.sleep(3 * 1000);
        String response_t2 = getTimeLastMedition(getResponse(sensor));
        System.out.println("status sensor "+response_t1.equals(response_t2));
        return response_t1.equals(response_t2);
    }
    private static String getResponse(Sensor sensor) throws IOException, InterruptedException {
        refreshToken(sensor);
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(sensor.getUrl_telemetria()))
                    .header("content-type", "application/json")
                    .header("x-authorization", "Bearer " + sensor.getToken())
                    .method("GET", HttpRequest.BodyPublishers.noBody())
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());

            getTimeLastMedition(response.body());
            return  response.body();
    }

    public static int getDistance(Sensor sensor) throws IOException, InterruptedException {
        refreshToken(sensor);
        return getDistances(getResponse(sensor));
    }

    public static String getTimeLastMedition(String response) {
        JSONObject jsonObject = new JSONObject(response);
        JSONArray lineItems = jsonObject.getJSONArray("distance");
        JSONObject jsonObject1 = (JSONObject) lineItems.get(0);
        return String.valueOf(jsonObject1.get("ts"));
    }
    private static int getDistances(String response) {
        JSONObject jsonObject = new JSONObject(response);
        JSONArray lineItems = jsonObject.getJSONArray("distance");
        JSONObject jsonObject1 = (JSONObject) lineItems.get(0);
        return Integer.parseInt(String.valueOf(jsonObject1.get("value")));
    }

    public static void refreshToken(Sensor sensor) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(sensor.getUrl_autenticacion()))
                .header("content-type", "application/json")
                .header("x-authorization", "Bearer "+sensor.getToken())
                .method("POST", HttpRequest.BodyPublishers.ofString("{\"refreshToken\":\""+sensor.getRefresh_token()+"\"}\n"))
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

        JSONObject jsonObject = new JSONObject(response.body());
        sensor.setToken(String.valueOf(jsonObject.get("token")));
    }

}
